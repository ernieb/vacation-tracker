//
//  AVacation.h
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AVacation : NSObject 
{
	NSDate *startDate;
	NSDate *endDate;
	NSString *description;
	
	BOOL debug;
}

@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, retain) NSString *description;
@property BOOL debug;

- (void) init:(NSDate *)start:(NSDate *)end:(NSString *)d;

- (int) calcWorkDaysInVacation;

@end
