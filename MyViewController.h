//
//  MyViewController.h
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import "TableViewController.h"


@interface MyViewController : UIViewController <UITextFieldDelegate>
{
	UILabel *label_title;	
	UILabel *label_vacationDays;
	UILabel *label_daysLeftUntilEOY;
	UILabel *label_vacationLeft;
	
	UITextField *textField;
	UISlider *vacaDaySlider;
	
	Model *dataModelReference;
}

@property (nonatomic, retain) IBOutlet UILabel *label_title;
@property (nonatomic, retain) IBOutlet UILabel *label_vacationDays;
@property (nonatomic, retain) IBOutlet UILabel *label_daysLeftUntilEOY;
@property (nonatomic, retain) IBOutlet UILabel *label_vacationLeft;

@property (nonatomic, retain) IBOutlet UITextField *textField;
@property (nonatomic, retain) IBOutlet UISlider *vacaDaySlider;

@property (nonatomic, retain) Model *dataModelReference;

- (IBAction) updateVacationDays:(id)sender;
- (void)updateVacationDaysLabel;

@end
