//
//  Model.m
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Model.h"

@implementation Model

@synthesize vacationDays;
@synthesize name;
@synthesize vacationLeft;
@synthesize weekdaysUntilEOY;
@synthesize logOn;
@synthesize vacations;

- (void) initWithMyDefaults
{
	logOn = NO;
	name = 85;
	vacationDays = 0;
	vacationLeft = 0;
	vacations = [[NSMutableArray alloc] init];
    weekdaysUntilEOY = [self calculateWeekdaysFrom:[NSDate date]];
    
//    // test code -- add 1 element to the vacations array
//    AVacation *v;
//    NSDate *s, *e;
//    NSString *descr = @"First Vacation";
//    
//    s = [NSDate date];
//    e = [NSDate date];
//    v = [AVacation alloc]; 
//    
//    [v init:s:e:descr];
//    
//    [self addVacation:v];
//    [v release];
//    // end test code
    
}

// Calculate weekdays until the end of the year
- (int) calculateWeekdaysFrom:(NSDate *) now
{
	int weekdays = 0;
	if (now == nil)
	{
		now = [NSDate date];
	}
    NSCalendar *calendar = [NSCalendar currentCalendar];
	
	NSDateComponents *nowComponents = 
    [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                fromDate:now];
    
	NSDateComponents *eoyComps = [[NSDateComponents alloc] init];
    [eoyComps setDay:31];
	[eoyComps setMonth:12];	
	[eoyComps setYear:nowComponents.year];
	
	NSDate *endOfYear = [calendar dateFromComponents:eoyComps];
	[eoyComps release];
	
	if (logOn) NSLog(@"Current date is %@. EOY is %@", now, endOfYear);
    
	NSUInteger calUnits = NSDayCalendarUnit;// | NSWeekdayCalendarUnit;
	
	NSDateComponents *untilEOYcomps = [calendar components:calUnits
                                                  fromDate:now
                                                    toDate:endOfYear
                                                   options:0];
	int days2EOY = [untilEOYcomps day];
    
	if (logOn) NSLog(@"Days until EOY is %d.", days2EOY);
    
	
	NSDateComponents *temp = [[NSDateComponents alloc] init];
    
	for( ; days2EOY >= 0; days2EOY--)
	{
		[temp setDay:days2EOY];
		NSDate *tempDate = [calendar dateByAddingComponents:temp toDate:now options:0];
		if (logOn) NSLog(@"Temp is %@. ", tempDate);
		
		NSDateComponents *t = [calendar components:NSWeekdayCalendarUnit fromDate:tempDate];
		if (logOn) NSLog(@"Weekday is %d.", t.weekday);
		
		if (t.weekday == 1 || t.weekday == 7)
		{
			continue;
		}
		weekdays++;
	}
	
	[temp release];
	
	if (logOn) NSLog(@"Calculated weekdays is %d.", weekdays);
	return weekdays;
}

- (void) addVacation:(AVacation *)aVacation
{
	if (aVacation == nil)
	{
        NSLog(@"Error. addVacation:AVacation is NIL");
		return;
	}
	
	[vacations addObject:aVacation];
    [self printVacationsArray];
}

- (void) removeVacation:(AVacation *)aVacation
{
	if (aVacation == nil)
	{
        NSLog(@"Error. removeVacation:AVacation is NIL");
		return;
	}
	
	[vacations removeObject:aVacation];
    [self printVacationsArray];
}

- (void) removeVacationByDescription:(NSString *)description
{
	if (description == nil)
	{
        NSLog(@"Error. removeVacation:description is NIL");
		return;
	}
    
    for(AVacation *v in vacations)
    {
        if ([v.description isEqualToString:description])
        {
            [vacations removeObject:v];
        }
    }
    [self printVacationsArray];
}

- (AVacation *) getVacationByDescription:(NSString *)description
{
	if (description == nil)
	{
        NSLog(@"Error. removeVacation:description is NIL");
		return nil;
	}
    
    for(AVacation *v in vacations)
    {
        if ([v.description isEqualToString:description])
        {
            return v;
        }
    }
    
    return nil;
}

- (AVacation *) getVacationAtIndex:(NSInteger) index
{
    return [vacations objectAtIndex:index];
}

-(int) getVacationsSize
{
    return vacations.count;
}

- (void) printVacationsArray
{
    if (logOn)
    {
        for (int i=0; i<vacations.count; i++)
        {
            AVacation *v = [vacations objectAtIndex:i];
            
            NSLog(@"nDescription: %@", v.description);
        }
    }
}


- (void) dealloc
{
	[vacations release];
	[super dealloc];
}

@end
