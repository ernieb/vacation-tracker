//
//  VacationTracker_1AppDelegate.h
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Model.h"
#import "MyViewController.h"
#import "TableViewController.h"

@class MyViewController;
@class Model;
@class TableViewController;

@interface VacationTracker_1AppDelegate : NSObject <UIApplicationDelegate> {
	
	UIWindow *window;
   MyViewController *myViewController;
   TableViewController *tableViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) MyViewController *myViewController;
@property (nonatomic, retain) TableViewController *tableViewController;

@end

