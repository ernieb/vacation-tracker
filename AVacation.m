//
//  AVacation.m
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AVacation.h"
@implementation AVacation

@synthesize startDate;
@synthesize endDate;
@synthesize description;
@synthesize debug;

- (void) init:(NSDate *)start:(NSDate *)end:(NSString *)d
{
	self = [super init];
	startDate = start;
	endDate = end;
	description = d;
	debug = NO;
}

- (int) calcWorkDaysInVacation
{
	if (self.startDate == nil || self.endDate == nil) return 0;
	
	int workDays = 0;
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSUInteger calUnits = NSDayCalendarUnit | NSWeekdayCalendarUnit;
	
	NSDateComponents *weekDaycomps = [calendar components:calUnits
															 fromDate:self.startDate
															 toDate:self.endDate
															 options:0];
	int daysInVaca = [weekDaycomps day];
	
	if (debug) NSLog(@"Days in vacation %d.", daysInVaca);
	
	
	NSDateComponents *temp = [[NSDateComponents alloc] init];
	
	for( ; daysInVaca >= 0; daysInVaca--)
	{
		[temp setDay:daysInVaca];
		NSDate *tempDate = [calendar dateByAddingComponents:temp toDate:self.startDate options:0];
		if (debug) NSLog(@"Temp is %@. ", tempDate);
		
		NSDateComponents *t = [calendar components:NSWeekdayCalendarUnit fromDate:tempDate];
		if (debug) NSLog(@"Weekday is %d.", t.weekday);
		
		if (t.weekday == 1 || t.weekday == 7)
		{
			continue;
		}
		workDays++;
	}
	
	[temp release];
	
	if (debug) NSLog(@"Calculated weekdays in vacation is %d.", workDays);
	
	return workDays;
}

- (void) dealloc
{
	[super dealloc];
}

@end
