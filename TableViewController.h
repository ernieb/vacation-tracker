//
//  TableViewController.h
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"


@interface TableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
   Model *dataModelReference;
}

@property (nonatomic, retain) Model *dataModelReference;



@end
