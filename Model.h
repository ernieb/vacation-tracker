//
//  Model.h
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVacation.h"

@interface Model : NSObject 
{
	BOOL logOn;

	int name;
	int vacationDays;
	int vacationLeft;
	int weekdaysUntilEOY;
	NSMutableArray *vacations;
	
}
@property BOOL logOn;

@property int vacationDays;
@property int name;
@property int vacationLeft;
@property int weekdaysUntilEOY;
@property (nonatomic, retain) NSMutableArray *vacations;

- (void) initWithMyDefaults;
- (int) calculateWeekdaysFrom:(NSDate *) now;
- (void) addVacation:(AVacation *)aVacation;
- (void) removeVacation:(AVacation *)aVacation;
- (void) removeVacationByDescription:(NSString *)description;
- (AVacation *) getVacationByDescription:(NSString *)description;
- (void) printVacationsArray;
- (int) getVacationsSize;
- (AVacation *) getVacationAtIndex:(NSInteger) index;

@end
