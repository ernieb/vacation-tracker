//
//  testNum1.m
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 1/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*
 January 2010
 S  M  T  W  Th F  S
                1  2
 3  4  5  6  7  8  9
 10 11 12 13 14 15 16
 17 18 19 20 21 22 23
 24 25 26 27 28 29 30
 31
 
 Feburary 2010
 S  M  T  W  Th F  S
    1  2  3  4  5  6
 7  8  9  10 11 12 13
 14 15 16 17 18 19 20
 21 22 23 24 25 26 27
 28
 
*/
#import <GHUnit/GHUnit.h>
#import "AVacation.h"

@interface VacationDateCalculations : GHTestCase { }
@end

@implementation VacationDateCalculations

- (AVacation*) allocVacationObject:(NSString *)start:(NSString *)end
{
   AVacation *av;
   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   [dateFormatter setDateStyle:NSDateFormatterShortStyle];
   NSDate *s = [dateFormatter dateFromString:start];
   NSDate *e = [dateFormatter dateFromString:end];
   [dateFormatter release];
   
   av = [AVacation alloc];
   [av init:s:e:@"UnitTest"];
   
   return av;
}

- (void) testSameStartAndEndWeekend
{
   AVacation *av = [self allocVacationObject:@"01/02/10":@"01/02/10"];
   
   // Calculate days
   int vacationDays = [av calcWorkDaysInVacation];
   
   GHAssertEquals(vacationDays, 0, @"Answer did not equal 0. Calculated value %d", vacationDays);
   [av release];
}

- (void) testSameStartAndEndWeekday
{
   AVacation *av = [self allocVacationObject:@"01/04/10":@"01/04/10"];
   
   // Calculate days
   int vacationDays = [av calcWorkDaysInVacation];
   
   GHAssertEquals(vacationDays, 1, @"Answer did not equal 1. Calculated value %d", vacationDays);
   [av release];
}

- (void) testEndDateInFuture
{
   AVacation *av = [self allocVacationObject:@"01/01/10":@"01/07/10"];

   // Calculate days
   int vacationDays = [av calcWorkDaysInVacation];
   
   GHAssertEquals(vacationDays, 5, @"Answer did not equal 5. Calculated value %d", vacationDays);
   
   [av release];
}

- (void) testEndDateInPast
{
   AVacation *av = [self allocVacationObject:@"01/07/10":@"01/01/10"];
   
   // Calculate days
   int vacationDays = [av calcWorkDaysInVacation];
   
   GHAssertEquals(vacationDays, 0, @"Answer did not equal 0. Calculated value %d", vacationDays);
   
   [av release];
}

- (void) testStartDateInJanEndDateInFeb
{
   AVacation *av = [self allocVacationObject:@"01/25/10":@"02/08/10"];
   
   // Calculate days
   int vacationDays = [av calcWorkDaysInVacation];
   
   GHAssertEquals(vacationDays, 11, @"Answer did not equal 11. Calculated value %d", vacationDays);
   
   [av release];
}



@end
