//
//  MyViewController.m
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MyViewController.h"


@implementation MyViewController;

@synthesize label_title;
@synthesize label_vacationDays;
@synthesize label_daysLeftUntilEOY;
@synthesize label_vacationLeft;

@synthesize textField;
@synthesize vacaDaySlider;
@synthesize dataModelReference;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
{
	[super viewDidLoad];
   	
	label_daysLeftUntilEOY.text = 
                [NSString stringWithFormat:@"%d", dataModelReference.weekdaysUntilEOY];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (IBAction) updateVacationDays:(id)sender
{
	dataModelReference.vacationDays = (int)(vacaDaySlider.value);
	
	//self.updateVacationDaysLabel;
}

-(void) updateVacationDaysLabel
{
	label_vacationDays.text = [NSString stringWithFormat:@"%d", dataModelReference.vacationDays];
}


- (void)dealloc 
{
	[label_title release];
	[label_vacationDays release];
	[label_daysLeftUntilEOY release];
	[label_vacationLeft release];
	
	[textField release];
	[vacaDaySlider release];
	[dataModelReference release];
	[super dealloc];
}


@end
