//
//  UTModel.m
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 1/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

// Used http://kalender-365.de/weekdays.php to generate working days in a period

#import <GHUnit/GHUnit.h>
#import "Model.h"

@interface UTModel : GHTestCase { }
@end

@implementation UTModel

- (NSDate *) getDateFromString:(NSString *)date
{
   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   [dateFormatter setDateStyle:NSDateFormatterShortStyle];
   NSDate *d = [dateFormatter dateFromString:date];
   [dateFormatter release];
   return d;
}

- (void) testCalWkdaysFromStartOfYear
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];
   
   NSDate *d = [self getDateFromString:@"01/01/10"];

   int weekdays = [m calculateWeekdaysFrom:d];
   GHAssertEquals(weekdays, 261, @"Answer did not equal 261. Calculated value %d", weekdays);
   [m release];
}

- (void) testCalWkdaysFromNotStartOfYear
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];
   NSDate *d = [self getDateFromString:@"06/01/10"];
   
   int weekdays = [m calculateWeekdaysFrom:d];
   
   GHAssertEquals(weekdays, 154, @"Answer did not equal 154 Calculated value %d", weekdays);
   [m release];
}

- (void) testCalWkdaysFromNearEndOfYear
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];
   NSDate *d = [self getDateFromString:@"12/01/10"];

   int weekdays = [m calculateWeekdaysFrom:d];
   
   GHAssertEquals(weekdays, 23, @"Answer did not equal 23 Calculated value %d", weekdays);
   [m release];
}

- (void) testCalWkdays
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];
   NSDate *d = [self getDateFromString:@"12/30/10"];

   int weekdays = [m calculateWeekdaysFrom:d];
   
   GHAssertEquals(weekdays, 2, @"Answer did not equal 2 Calculated value %d", weekdays);
   [m release];
}

- (void) testAddVacation
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];

   AVacation *v;
   NSDate *s, *e;
   NSString *descr = @"Ernie";
   NSString *d;
   
   s = [NSDate date];
   e = [NSDate date];
   
   
   for (int i=0; i<8; i++)
   {
      v = nil;
      v = [AVacation alloc]; 
      d = [NSString stringWithFormat:@"%@ (%d)", descr, i ];
      
      [v init:s:e:d];
      
      [m addVacation:v];
   }
    [m setLogOn:YES];
   [m printVacationsArray];
   GHAssertEquals([m getVacationsSize], 8, nil);

   [m release];
}

- (void) testRemoveVacation
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];

   AVacation *v;
   NSDate *s, *e;
   NSString *descr = @"Ernie";
   NSString *d;
   
   s = [NSDate date];
   e = [NSDate date];
   
   
   for (int i=0; i<8; i++)
   {
      v = nil;
      v = [AVacation alloc]; 
      d = [NSString stringWithFormat:@"%@ (%d)", descr, i ];
      
      [v init:s:e:d];
      
      [m addVacation:v];
   }
  
   GHAssertEquals([m getVacationsSize], 8, nil);

   AVacation *removeMe = [m getVacationByDescription:d];

   [m removeVacation:removeMe];
   GHAssertEquals([m getVacationsSize], 7, nil);

   [m release];
}

- (void) testRemoveVacationByDescription
{
   Model *m = [Model alloc];
   [m initWithMyDefaults];

   AVacation *v;
   NSDate *s, *e;
   NSString *descr = @"Ernie";
   NSString *d;
   
   s = [NSDate date];
   e = [NSDate date];
   
   
   for (int i=0; i<8; i++)
   {
      v = nil;
      v = [AVacation alloc]; 
      d = [NSString stringWithFormat:@"%@ (%d)", descr, i ];
      
      [v init:s:e:d];
      
      [m addVacation:v];
      [v release];
   }
  
   GHAssertEquals([m getVacationsSize], 8, nil);

   [m removeVacationByDescription:d];
   GHAssertEquals([m getVacationsSize], 7, nil);

   [m release];
}
@end
