//
//  TableViewController.m
//  VacationTracker_1
//
//  Created by Ernie Biancarelli on 11/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TableViewController.h"


@implementation TableViewController
@synthesize dataModelReference;

/*
- (void)loadView
{
   
   CGRect rect;
	rect.origin.x = 0;
	rect.origin.y = 221;
	rect.size.width = 320;
	rect.size.height = 240;
	

	UITableView *uiTable = [[UITableView alloc] initWithFrame:rect
                                            style:UITableViewStylePlain];
	
	//vacaTable.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
	uiTable.delegate = self;
	uiTable.dataSource = dataSrc;
   
	[uiTable reloadData];
   
   self.view = uiTable;
   [uiTable release];
   
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // Number of rows is the number of vacations
   return (NSInteger)dataModelReference.getVacationsSize;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *MyIdentifier = @"CellForVacaTable";
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
   if (cell == nil) 
   {
      cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
                                                  reuseIdentifier:MyIdentifier] autorelease];
   }
   
   AVacation *v = [dataModelReference getVacationAtIndex:indexPath.row];
   
   if (v == nil)
   {   
      //NSLog(@"IndexPath.row(%d): Nil Object", indexPath.row);
      cell.textLabel.text = @"Nil";
   }
   else
   {
      //NSLog(@"IndexPath.row(%d): %@", indexPath.row, v.description);
      cell.textLabel.text = v.description;
   }
   return cell;
}
- (void)dealloc 
{
    [super dealloc];
}


@end

